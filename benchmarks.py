#!/usr/bin/python3
import logging
import time

from dofbot import DofBot
from dofbot.utils import Area

if __name__ == "__main__":

    format = "[%(asctime)-15s][%(levelname).1s] %(filename)s:%(lineno)s - %(funcName)s() - %(message)s"
    logging.basicConfig(format=format)

    bot = DofBot(go_left_area=Area(0, 510, 360, 916), go_right_area=Area(1560, 67, 1920, 714), go_down_area=Area(832, 897, 1550, 916))



    timer1 = time.time()
    for _ in range(0,100):
        bot.is_black_screen()
    print(time.time() - timer1)

    timer2 = time.time()
    for _ in range(0,100):
        bot.is_black_screen_old()
    print(time.time() - timer2)