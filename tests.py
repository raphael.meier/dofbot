#!/usr/bin/python3
import argparse
import logging
import time

import pyautogui
import numpy
import cv2
import PIL

from dofbot import DofBot
from dofbot.utils import Area

if __name__ == "__main__":
    time.sleep(3)

    format = "[%(asctime)-15s][%(levelname).1s] %(filename)s:%(lineno)s - %(funcName)s() - %(message)s"
    logging.basicConfig(format=format)


    tested = cv2.imread("./tmp/workdir/tested.png")
    true = cv2.imread("./tmp/workdir/true.png")
    false = cv2.imread("./tmp/workdir/false.png")

    gray_tested = cv2.cvtColor(tested, cv2.COLOR_BGR2GRAY)
    x = cv2.Sobel(gray_tested, cv2.CV_64F, 1,0, ksize=3, scale=1)
    y = cv2.Sobel(gray_tested, cv2.CV_64F, 0,1, ksize=3, scale=1)
    absx= cv2.convertScaleAbs(x)
    absy = cv2.convertScaleAbs(y)
    edge_tested = cv2.addWeighted(absx, 0.5, absy, 0.5,0)

    gray_true = cv2.cvtColor(true, cv2.COLOR_BGR2GRAY)
    x = cv2.Sobel(gray_true, cv2.CV_64F, 1,0, ksize=3, scale=1)
    y = cv2.Sobel(gray_true, cv2.CV_64F, 0,1, ksize=3, scale=1)
    absx= cv2.convertScaleAbs(x)
    absy = cv2.convertScaleAbs(y)
    edge_true = cv2.addWeighted(absx, 0.5, absy, 0.5,0)

    gray_false = cv2.cvtColor(false, cv2.COLOR_BGR2GRAY)
    x = cv2.Sobel(gray_false, cv2.CV_64F, 1,0, ksize=3, scale=1)
    y = cv2.Sobel(gray_false, cv2.CV_64F, 0,1, ksize=3, scale=1)
    absx= cv2.convertScaleAbs(x)
    absy = cv2.convertScaleAbs(y)
    edge_false = cv2.addWeighted(absx, 0.5, absy, 0.5,0)

    diff1 = cv2.subtract(edge_tested, edge_true)
    diff2 = cv2.subtract(edge_tested, edge_false)
    diff3 = cv2.subtract(edge_true, edge_false)

    print(numpy.sum((edge_tested.astype("float") - edge_true.astype("float")) ** 2) / float(edge_tested.shape[0] * edge_tested.shape[1]))
    print(numpy.sum((edge_tested.astype("float") - edge_false.astype("float")) ** 2) / float(edge_tested.shape[0] * edge_tested.shape[1]))

    #print(cv2.compare(edge_tested,edge_true, ))
    # cv2.imshow('tested', edge_tested)
    # cv2.imshow('true', edge_true)
    # cv2.imshow('false', edge_false)
    # cv2.imshow('diff1', diff1)
    # cv2.imshow('diff2', diff2)
    # cv2.imshow('diff3', diff3)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    coord = pyautogui.locateOnScreen(edge, grayscale=True, confidence=0.7)
    coord2 = pyautogui.locateOnScreen(true, grayscale=False, confidence=0.95)
    print(coord)
    print(coord2)
    pyautogui.moveTo(coord)
    #pyautogui.moveTo(coord2)
