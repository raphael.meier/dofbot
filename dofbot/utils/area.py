#!/usr/bin/python3
from random import randint
from typing import Tuple


class Area():
    def __init__(self, left: int, top: int, right: int, bottom: int):
        self.left = left
        self.right = right
        self.top = top
        self.bottom = bottom

    def contain(self, x: int, y: int, padding: int = 0) -> bool:
        return self.left + padding < x < self.right - padding and self.top + padding < y < self.bottom - padding
    
    def to_tuple(self) -> Tuple[int, int, int, int]:
        return self.left, self.top, self.right, self.bottom

    def to_box_tuple(self) -> Tuple[int, int, int, int]:
        return self.left, self.top, self.right - self.left, self.bottom - self.top

    def random_pixel_in(self) -> Tuple[int, int]:
        x = randint(self.left + 1, self.right - 1)
        y = randint(self.top + 1, self.bottom - 1)
        return (x, y)

    @staticmethod
    def from_box(box):
        return Area(box[0], box[1], box[0] + box[2], box[1] + box[3])