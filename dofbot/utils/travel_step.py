#!/usr/bin/python3
from typing import List
from pyautogui import Point

from .area import Area

class TravelStep():
    def __init__(self, direction: str, change_map_area: Area = None, special_resources : List[Point] = []):
        self.direction = direction
        self.change_map_area = change_map_area
        self.special_resources = special_resources
    
    def opposite(self) -> str:
        if self.direction == "left":
            return "right"
        elif self.direction == "right":
            return "left"
        elif self.direction == "up":
            return "down"
        elif self.direction == "down":
            return "up"