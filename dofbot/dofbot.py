#!/usr/bin/python3
import logging
import math
import itertools
from random import randint
import time
import os
from typing import List, Tuple

import simplejson

import pyautogui

import win32api

from scipy.cluster.hierarchy import fclusterdata

from .utils.area import Area
from .utils.travel_step import TravelStep
from .dofbot_exception import DofBotException


class DofBot:
    def __init__(
        self,
        work_directory: str = "./tmp",
        images_directory: str = "./dofbot/resources/images",
        game_screen: Area = Area(0, 29, 1920, 916),
        map_area: Area = Area(365, 45, 1552, 893),
        map_coordinates_area: Area = Area(10, 75, 92, 100),
        map_witness_area: Area = Area(1590, 70, 1690, 170), # A square inside the right map change pannel
        go_left_area: Area = None,
        go_right_area: Area = None,
        go_up_area: Area = None,
        go_down_area: Area = None,
        fight_stuff_key: str = "1",
        farm_stuff_key: str = "2",
        callback_potion_key: str = "3",
        debug_mode: bool = False
    ):
        if not os.path.exists(work_directory):
            os.makedirs(work_directory)
            logging.info("Work directory created")

        self.work_directory = work_directory
        self.images_directory = images_directory
        self.game_screen = game_screen
        self.map_area = map_area
        self.farm_travel = []
        self.current_step = 0
        self.harvested_resources = []

        self.map_coordinates_area = map_coordinates_area
        self.map_witness_area = map_witness_area
        self.map_witness = None

        # Custom deplacement area
        self.go_left_area = go_left_area
        self.go_right_area = go_right_area
        self.go_up_area = go_up_area
        self.go_down_area = go_down_area

        self.fight_stuff_key = fight_stuff_key
        self.farm_stuff_key = farm_stuff_key
        self.callback_potion_key = callback_potion_key

        self.debug_mode = debug_mode

        self.avatar_stopped = False

        self.map_timeout = 60

    def routine(self):

        if not self.farm_travel:
            logging.error("No farm travel loaded")
            raise (DofBotException("No farm travel loaded"))

        permanent_mode = True

        self.reset_mouse_position()
        # Init map coordinates
        self.save_map_witness()

        while permanent_mode:
            logging.info("---- Iteration begining ----")
            logging.info("Harvesting")
            
            resources = []

            # Fisherman
            if not self.harvested_resources or "big_fish" in self.harvested_resources:
                resources += self.locate_resources("otomai_beach_big_fish.png", cluster_treshold=20, confidence=0.6) # Verified for village
                resources += self.locate_resources("pandala_big_fish.png", cluster_treshold=20, confidence=0.5) # Verified
                resources += self.locate_resources("frigost_big_fish.png", cluster_treshold=20, confidence=0.85) # Very light frigost fish
            if not self.harvested_resources or "middle_fish" in self.harvested_resources:
                resources += self.locate_resources("otomai_beach_middle_fish.png", cluster_treshold=20, confidence=0.6) # Verified for village
                resources += self.locate_resources("pandala_middle_fish.png", cluster_treshold=20, confidence=0.55)
            
            if not self.harvested_resources or "small_fish" in self.harvested_resources:
                resources += self.locate_resources("otomai_beach_small_fish.png", cluster_treshold=20, confidence=0.6)
                resources += self.locate_resources("astrub_small_fish.png", cluster_treshold=10, confidence=0.5)

            # Alchimiste
            if not self.harvested_resources or "pandouille" in self.harvested_resources:
                resources += self.locate_resources("pandouille.png", cluster_treshold=100, confidence=0.5, grayscale=False) # Verified
            if not self.harvested_resources or "belladone" in self.harvested_resources:
                resources += self.locate_resources("belladone.png", cluster_treshold=100, confidence=0.5, grayscale=False)
            if not self.harvested_resources or "perce_neige" in self.harvested_resources:
                resources += self.locate_resources("perce_neige.png", cluster_treshold=100, confidence=0.7, grayscale=False)
            if not self.harvested_resources or "orchyde" in self.harvested_resources:
                resources += self.locate_resources("orchyde.png", cluster_treshold=100, confidence=0.7, grayscale=False)
            if not self.harvested_resources or "ortie" in self.harvested_resources:
                resources += self.locate_resources("ortie.png", cluster_treshold=100, confidence=0.8, grayscale=False)

            # Special resource point
            if self.farm_travel[self.current_step].special_resources:
               resources += self.farm_travel[self.current_step].special_resources

            resources = self.clusterise_points(resources, 20)

            exit_point = self.get_map_exit_point()

            resources = self.build_fastest_harvest_path(resources, exit_point)

            self.harvest(resources)

            self.leave_map(exit_point)

            logging.info(f"Going to {self.farm_travel[self.current_step].direction} with point {exit_point}. Step number {self.current_step}")

            self.reset_mouse_position()

            logging.info("Waiting for events")
            timer_begin = time.time()
            while True:
              
                # Fight 
                if self.fight_engaged():
                    logging.info("Fight engaged")
                    if self.debug_mode:
                        pyautogui.screenshot(os.path.join(self.work_directory, "history", f"fight_engaged{time.time()}.png"))
                    self.equip_stuff("fight")
                    self.fight_routine()
                    # To avoid the map change if fight engaged
                    self.stop_avatar()
                    self.equip_stuff("farm")
                    break

                # Full pods
                elif self.is_full_pods():
                    logging.info("Full pods, going to bank or level up")
                    # Close full pods
                    self.close_harvesting_impossible_alert()

                    # Exit game turn of pc
                    # Close dofus
                    # pyautogui.hotkey("alt", "f4")
                    # # Close bot
                    # pyautogui.hotkey("alt", "f4")
                    # # Open turn of pc
                    # pyautogui.hotkey("alt", "f4")
                    # pyautogui.press("enter")
                    continue
                    # Rappel
                    pyautogui.press(self.callback_potion_key)
                    # Dummy travel to Bank
                    self.go_left()
                    # Enter and open bank
                    # self.click()
                    # Empty resources

                    # Close bank
                    pyautogui.press("esc")
                    # Go to closest Zaap

                    break

                # Map change
                elif self.map_changed() and not self.fight_engaged():
                    logging.info("Map changed")
                    
                    self.incremente_current_step()

                    # Wait for dofus to update map coordinate
                    time.sleep(1)
                    self.save_map_witness()
                
                    # History logs
                    if self.debug_mode:
                        ...
                        #pyautogui.screenshot(os.path.join(self.work_directory, "history", f"map_changed_{time.time()}.png"))
                    break

                # Avatar has been stopped
                elif self.avatar_stopped:
                    logging.debug("Avatar stopped")
                    self.avatar_stopped = False
                    break

                # Nothing happened since 1 minute
                if self.map_timeout <= time.time() - timer_begin:
                    logging.warn("Nothing happened since %s seconds", time.time() - timer_begin)
                    if self.debug_mode:
                        pyautogui.screenshot(os.path.join(self.work_directory, "history", f"nothing_happened_{time.time()}_{self.farm_travel[self.current_step].direction}.png"))
                    timer = 0
                    break
                

    # Harvesting
    def harvest(self, resources: List[pyautogui.Point]):
        for point in resources:
            self.click(point[0], point[1], shift_click=True)

    def locate_resources(
        self, resource_sprite_file_name: str, cluster_treshold: int = 10, confidence: int = 0.35, grayscale: bool = True,
    ) -> List[pyautogui.Point]:

        points = []
        for resource in pyautogui.locateAllOnScreen(
                os.path.join(self.images_directory, 'resources', resource_sprite_file_name), grayscale=grayscale, confidence=confidence
        ):
            point = pyautogui.center(resource)
            if self.map_area.contain(x=point.x, y=point.y, padding=5):
                points.append(point)

        return self.clusterise_points(points, cluster_treshold)

    def clusterise_points(self, points: List[pyautogui.Point], cluster_treshold: int = 10) -> List[pyautogui.Point]:

        if not points:
            return []
        elif len(points) == 1:
            return points

        # Clustering
        clusters = fclusterdata(points, cluster_treshold, criterion="distance")
        results = []
        past_values = []
        for pos, value in enumerate(clusters):
            if value not in past_values:
                results.append(points[pos])
                past_values.append(value)
        
        return results

    def build_fastest_harvest_path(self, points: List[pyautogui.Point], exit_point: pyautogui.Point) -> List[pyautogui.Point]:

        # Generate each possible paths
        paths = [ p for p in itertools.permutations(points) ]

        # Calculate the length of each path + the leaving point of the map 
        path_distances = [ sum(map(lambda x: math.hypot(x[1][0]-x[0][0],x[1][1]-x[0][1]), zip(p, p[1:] + (exit_point,)))) for p in paths ]

        min_index = 0
        min_path_distance = path_distances[0]
        for i, path_distance in enumerate(path_distances):
            if path_distance < min_path_distance:
                min_path_distance = path_distance
                min_index = i
        return paths[min_index]

    # Basic action

    def click(self, x: int, y: int, shift_click: bool = False):
        if shift_click:
            pyautogui.keyDown("shiftleft")
        pyautogui.click(x, y)
        if shift_click:
            pyautogui.keyUp("shiftleft")
        logging.debug(f"Click at {(x, y)}")

    def click_in(self, area: Area, shift_click: bool = False):
        x = randint(area.left + 1, area.right - 1)
        y = randint(area.top + 1, area.bottom - 1)
        self.click(x, y, shift_click)

    def reset_mouse_position(self):
        pyautogui.moveTo(5, 5)

    def screenshot(self, file_name: str = None, region: Tuple = None):
        screenshot = pyautogui.screenshot(region=region)
        logging.debug("Screenshot took")
        if file_name:
            screenshot_path = os.path.join(self.work_directory, file_name)
            logging.info("Screenshot saved at %s", screenshot_path)
        return screenshot

    def get_up_exit_point(self) -> pyautogui.Point:
        if not self.go_up_area:
            area = Area(
                self.map_area.left,
                self.game_screen.top,
                self.map_area.right,
                self.map_area.top,
            )
        else:
            area = self.go_up_area
        return pyautogui.Point(randint(area.left + 1, area.right - 1), randint(area.top + 1, area.bottom - 1))

    def get_down_exit_point(self) -> pyautogui.Point:
        if not self.go_down_area:
            area = Area(
                self.map_area.left,
                self.map_area.bottom,
                self.map_area.right,
                self.game_screen.bottom,
            )
        else:
            area = self.go_down_area
        return pyautogui.Point(randint(area.left + 1, area.right - 1), randint(area.top + 1, area.bottom - 1))

    def get_left_exit_point(self) -> pyautogui.Point:
        if not self.go_left_area:
            area = Area(
                self.game_screen.left,
                self.game_screen.top,
                self.map_area.left,
                self.game_screen.bottom,
            )
        else:
            area = self.go_left_area
        return pyautogui.Point(randint(area.left + 1, area.right - 1), randint(area.top + 1, area.bottom - 1))

    def get_right_exit_point(self) -> pyautogui.Point:
        if not self.go_right_area:
            area = Area(
                self.map_area.right,
                self.game_screen.top,
                self.game_screen.right,
                self.game_screen.bottom,
            )
        else:
            area = self.go_right_area
        return pyautogui.Point(randint(area.left + 1, area.right - 1), randint(area.top + 1, area.bottom - 1))

    def set_keyboard_layout_to_english(self):
        # Add the english keyboard
        win32api.LoadKeyboardLayout("00000809",1)

        # Set it
        pyautogui.keyDown("alt")
        pyautogui.keyDown("shiftleft")
        pyautogui.keyUp("shiftleft")
        while not pyautogui.locateOnScreen(os.path.join(self.images_directory, "windows", "keyboard_english_small.png"), confidence=0.95):
            pyautogui.press("shiftleft")
        pyautogui.keyUp("alt")
        logging.info("Keyboard set to english")
        


    def equip_stuff(self, stuff_type: str = "fight"):
        if stuff_type == "fight":
            pyautogui.press(self.fight_stuff_key)
        elif stuff_type == "farm":
            pyautogui.press(self.farm_stuff_key)
        
        # Verify warning message
        time.sleep(1)
        warning = pyautogui.locateCenterOnScreen(
                os.path.join(self.images_directory, "interface", "ok_button.png"), confidence=0.9
            )
        if warning:
            self.click(warning.x, warning.y)

    def find_close_button(self) -> Area:
        cross = pyautogui.locateOnScreen(os.path.join(self.images_directory, "interface", "close_button.png"), confidence=0.9)
        if cross:
            return Area.from_box(cross)
        else:
            return None

    def close_harvesting_impossible_alert(self):
        cross = self.find_close_button()
        if cross:
            self.click_in(cross)
            self.reset_mouse_position()
            logging.debug("Closing harvesting impossible alert")
        else:
            logging.warn("Can't find harvesting impossible alert closing button")

    def stop_avatar(self):
        # The avatar is stopped by clicking to next map
        exit_point = self.get_map_exit_point()
        self.leave_map(exit_point, shift_click=False)

        # If the avatar is harvesting he will stop just after, if not he will change map
        self.reset_mouse_position()
        time.sleep(7)

        # Detect the case he moved
        if self.map_changed() and not self.fight_engaged() :
            logging.info("Map changed")
            self.incremente_current_step()
            self.save_map_witness()
        logging.debug("Avatar stopped")


    # Travel
    def get_map_exit_point(self) -> pyautogui.Point:
        special_exit_area = self.farm_travel[self.current_step].change_map_area
        if special_exit_area:
            point = pyautogui.Point(randint(special_exit_area.left + 1, special_exit_area.right - 1), randint(special_exit_area.top + 1, special_exit_area.bottom - 1))
        else:
            direction = self.farm_travel[self.current_step].direction
            if direction == "left":
                point = self.get_left_exit_point()
            elif direction == "right":
                point = self.get_right_exit_point()
            elif direction == "up":
                point = self.get_up_exit_point()
            elif direction == "down":
                point = self.get_down_exit_point()
        return point

    def leave_map(self, exit_point: pyautogui.Point, shift_click=True):
        self.click(exit_point.x, exit_point.y, shift_click=shift_click)
        

    def incremente_current_step(self):
        self.current_step += 1

        # End of the travel
        if self.current_step == len(self.farm_travel):
            self.current_step = 0

    def load_farm_travel(self, farm_travel_path: str):
        try:
            with open(farm_travel_path) as farm_travel_file:
                farm_travel_json = simplejson.load(farm_travel_file)
                
                if "harvested_resources" not in farm_travel_json:
                    logging.warning("No harvested resources in json. Harvest everything")
                    self.harvested_resources = []
                else:
                    logging.warning("Harvested resources: %s", farm_travel_json["harvested_resources"])
                    self.harvested_resources = farm_travel_json["harvested_resources"]

                for step in farm_travel_json["path"]:
                    change_map_area = None
                    special_resources = []
                    if "change_map_area" in step:
                        change_map_area = Area(
                            step[change_map_area][0],
                            step[change_map_area][1],
                            step[change_map_area][2],
                            step[change_map_area][3],
                        )
                    if "special_resources" in step:
                        for resource in step["special_resources"]:
                            special_resources.append(pyautogui.Point(resource[0] + randint(-5,5), resource[1] + randint(-5,5)))

                    self.farm_travel.append(TravelStep(step["direction"], change_map_area, special_resources))

            if not self._travel_is_loop(self.farm_travel):
                logging.debug("Travel is not a loop: adding the way back")

                special_resources = []
                for step in reversed(self.farm_travel):
                    self.farm_travel.append(TravelStep(step.opposite(), special_resources=special_resources))

                    # In reverse, the special_resources points are in step + 1 and not step
                    if special_resources:
                        # Reset special_resources
                        special_resources = []
                    if step.special_resources:
                        # Save special_resources to add it to step + 1
                        special_resources = step.special_resources

        except FileNotFoundError as e:
            logging.error(f"File not found at {farm_travel_path}")
            raise e
        except KeyError as e:
            logging.error(f"Json error inside {farm_travel_path}")
            raise e

        logging.info(f"Travel at {farm_travel_path} is loaded")

    def _travel_is_loop(self, travel: List) -> bool:
        total_x = 0
        total_y = 0
        for step in travel:
            if step.direction == "left":
                total_x -= 1
            elif step.direction == "right":
                total_x += 1
            elif step.direction == "up":
                total_y -= 1
            elif step.direction == "down":
                total_y += 1
        return total_x == 0 and total_y == 0

    # Map changement
    def save_map_witness(self):
        self.map_witness = self.screenshot(region=self.map_witness_area.to_box_tuple())
        if self.debug_mode:
            self.map_witness.save(os.path.join(self.work_directory, "map_witness", f"witness_{time.time()}.png"))

    def map_changed(self) -> bool:
        black = self.is_black_screen()
        located = pyautogui.locateOnScreen(self.map_witness, confidence=0.95, grayscale=False, region=self.map_witness_area.to_box_tuple())
        # print("located", located)
        # print("black", black)
        
        return False if black or located else True

    def is_black_screen_old(self) -> bool:
        black = pyautogui.locateOnScreen(os.path.join(self.images_directory, "interface", "black_screen.png"), confidence=0.98)
        return True if black else False

    def is_black_screen(self, precision: int = 10) -> bool:
        #witness = pyautogui.screenshot(region=self.map_witness_area.to_box_tuple())
        #black = len(witness.getcolors()) == 1 and witness.getcolors()[0][1] == (0, 0, 0)
        color_value = 0
        for _ in range(0,precision):
            pixel = self.map_area.random_pixel_in()
            color_value += sum(pyautogui.pixel(pixel[0], pixel[1]))/3
        return True if color_value / precision == 0 else False

    def is_full_pods(self) -> bool:
        return True if pyautogui.locateOnScreen(os.path.join(self.images_directory, "interface", "full_pods.png"), confidence=0.9) else False

    # Fight
    def fight_engaged(self) -> bool:
        return (
            True
            if pyautogui.locateOnScreen(
                os.path.join(self.images_directory, "fight", "ready_button.png"), confidence=0.9
            )
            else False
        )

    def fight_ended(self) -> bool:
        return (
            True
            if pyautogui.locateOnScreen(
                os.path.join(self.images_directory, "fight", "close_fight.png"), confidence=0.9
            )
            else False
        )

    def launch_fight(self):
        pyautogui.press("f1")

    def locate_enemies(self) -> List:
        enemies_list = []
        enemies_list += pyautogui.locateAllOnScreen(
            os.path.join(self.images_directory, "fight", "enemy_left.png"), grayscale=True, confidence=0.8
        )
        enemies_list += pyautogui.locateAllOnScreen(
            os.path.join(self.images_directory, "fight", "enemy_front.png"), grayscale=True, confidence=0.8
        )
        enemies_list += pyautogui.locateAllOnScreen(
            os.path.join(self.images_directory, "fight", "enemy_right.png"), grayscale=True, confidence=0.8
        )
        enemies_list += pyautogui.locateAllOnScreen(
            os.path.join(self.images_directory, "fight", "enemy_back.png"), grayscale=True, confidence=0.8
        )

        enemy_points = [pyautogui.center(enemy) for enemy in enemies_list]

        return self.clusterise_points(enemy_points, 10)

    def fire_spell(self, enemy_position: Tuple):
        pyautogui.press("4")
        self.click(enemy_position[0], enemy_position[1])

    def pass_turn(self):
        pyautogui.press("f1")

    def active_creature_mode(self):
        pyautogui.keyDown("shiftleft")
        pyautogui.press("5")
        pyautogui.keyUp("shiftleft")

    def fight_routine(self):
        logging.debug("Fight mode starts")
        self.launch_fight()
        time.sleep(1)
        while not self.fight_ended():
            self.reset_mouse_position()
            
            enemies = self.locate_enemies()

            if not enemies:
                self.active_creature_mode()
                enemies = self.locate_enemies()

            for enemy in enemies:
                time.sleep(1)
                self.fire_spell(enemy)

            # Case of only one enemy
            if len(enemies) == 1:
                self.fire_spell(enemies[0])
                time.sleep(0.58)
                self.fire_spell(enemies[0])

            self.pass_turn()
            time.sleep(3)

        logging.info("Fight ended")
        self.close_fight_result()

    def close_fight_result(self):
        close = pyautogui.locateCenterOnScreen(os.path.join(self.images_directory, "fight", "close_fight.png"), confidence=0.9)
        self.click(close.x, close.y)
        self.reset_mouse_position()
