#!/usr/bin/python3
import argparse
import logging
import time

from dofbot import DofBot
from dofbot.utils import Area

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--debug", dest="debug", help="Enable debug mode", action='store_true')

    args = parser.parse_args()

    debug = args.debug

    time.sleep(3)

    format = "[%(asctime)-15s][%(levelname).1s] %(filename)s:%(lineno)s - %(funcName)s() - %(message)s"
    if debug:
        logging.basicConfig(format=format, filename="dofbot.logs")
    else:
        logging.basicConfig(format=format)
    logging.getLogger().setLevel(logging.DEBUG)

    #farm_travel_path = "./dofbot/resources/farm_loops/astrub.json"
    
    farm_travel_path = "./dofbot/resources/farm_loops/otomai_seaside_village.json"
    farm_travel_path = "./dofbot/resources/farm_loops/otomai_seaside.json"
    farm_travel_path = "./dofbot/resources/farm_loops/frigost.json"
    farm_travel_path = "./dofbot/resources/farm_loops/debug.json"
    farm_travel_path = "./dofbot/resources/farm_loops/left_farmers_village.json"
    farm_travel_path = "./dofbot/resources/farm_loops/nimotopia.json"
    farm_travel_path = "./dofbot/resources/farm_loops/akwadala.json"

    bot = DofBot(go_left_area=Area(0, 510, 360, 916), go_right_area=Area(1560, 67, 1920, 714), go_down_area=Area(832, 897, 1550, 916), debug_mode=debug)
    #bot.set_keyboard_layout_to_english()
    bot.load_farm_travel(farm_travel_path)

    
    bot.routine()
    #pyautogui.moveTo(coord2)
    #print(bot.clusterise_points(bot.locate_enemies()))
